import pandas as pd
import sys

from LinearRegression import LinearRegression

if len(sys.argv) < 2:
	print ("usage: python training.py dataset.csv")
	sys.exit()

try:
	df = pd.read_csv(sys.argv[1])
except IOError as e:
	print ("Cannot open or / and read the file '" + sys.argv[1] + "'")
	sys.exit()
except pd.errors.EmptyDataError:
	print ("Invalid data file '" + sys.argv[1] + "'")
	sys.exit()
except pd.errors.ParserError:
	print ("Invalid data file '" + sys.argv[1] + "'")
	sys.exit()
except UnicodeDecodeError:
	print ("Invalid data file '" + sys.argv[1] + "'")
	sys.exit()

ln = LinearRegression()
if not ln.checkIntegrity(df):
	print ("Wrong dataset format. Please use .csv with ',' as separator")
	sys.exit()
ln.calculateTheta0AndThetha1(df)
ln.write(sys.argv[1])

ln.precision(df)
