import platform
import os
import pickle

class LinearRegression:
	filePath = ""
	theta0 = 0
	theta1 = 0
	thetasList = {}

	def __init__(self):
		self.theta0 = 0
		self.theta1 = 0
		system = platform.system()
		if system == "Linux" or system == "Darwin":
			self.filePath = "/tmp/linearRegression.lr"
		else:
			self.filePath = "C:\\Users\\" + os.getlogin() + "\\AppData\\Local\\Temp\\linearRegression.lr"

	def load(self):
		try:
			openedFile = open(self.filePath, "rb")
			self.thetasList = pickle.load(openedFile)
			openedFile.close()
			return True
		except IOError:
			return False
		except UnicodeDecodeError:
			print ("Data file corrumpted")
			raise ValueError
		except:
			print ("Data file corrumpted")
			raise ValueError


	def setThetasFromFilename(self, fileName):
		try:
			self.theta0 = self.thetasList[os.path.basename(fileName)][0]
			self.theta1 = self.thetasList[os.path.basename(fileName)][1]
		except KeyError:
			print ("The algorithm is not trained for that dataset yet")
			self.theta0 = 0
			self.theta1 = 0

	def selectThetas(self):
		for idx, thetas in enumerate(self.thetasList):
			print (str(idx + 1) + ": " + thetas)
		correct = False
		if len(self.thetasList) == 1:
			self.theta0 = list(self.thetasList.values())[0][0]
			self.theta1 = list(self.thetasList.values())[0][1]
			print("theta0 = " + str(round(self.theta0, 4)) + ", theta1 = " + str(round(self.theta1, 4)))
		else:
			while not correct and len(self.thetasList) > 0:
				try:
					value = int(input('Please select a dataset: '))
					if value <= 0 or value > len(self.thetasList):
						continue
					else:
						self.theta0 = list(self.thetasList.values())[value - 1][0]
						self.theta1 = list(self.thetasList.values())[value - 1][1]
						print("theta0 = " + str(round(self.theta0, 4)) + ", theta1 = " + str(round(self.theta1, 4)))
						correct = True
				except ValueError:
					print ("ValueError: Please enter an number")

	def write(self, fileName):
		try:
			self.load()
		except ValueError:
			self.thetasList = {}
		self.thetasList[os.path.basename(fileName)] = [self.theta0, self.theta1]
		try:
			openedFile = open(self.filePath, "wb")

			pickle.dump(self.thetasList, openedFile)
			openedFile.close()
		except IOError as e:
			print ("Cannot write the DataFile: " + str(e))

	def estimate(self, value):
		return self.theta0 + self.theta1 * value

	def dataScaling(self, minX, maxX, data):
		newData = []
		if minX == 0 and maxX == 0:
			for i in range(0, data.shape[0]):
				newData.append([data.iloc[i][0], data.iloc[i][1]])
		else:
			for i in range(0, data.shape[0]):
				newData.append([float((data.iloc[i][0] / (maxX - minX))), data.iloc[i][1]])
		return newData

	def checkIntegrity(self, data):
		if data.shape[0] == 0:
			return False
		for i in range(0, data.shape[0]):
			try:
				i0 = float(data.iloc[i][0])
				i1 = float(data.iloc[i][1])
				if i0 != i0 or i1 != i1:
					return False
			except ValueError:
				return False
			except IndexError:
				return False
		return True

	def precision(self, data):
		sse = 0
		sst = 0
		average = 0
		for i in range(0, data.shape[0]):
			average = average + data.iloc[i][1]
		average = average / data.shape[0]
		for i in range(0, data.shape[0]):
			sse = sse + (data.iloc[i][1] - self.estimate(data.iloc[i][0])) ** 2
			sst = sst + (data.iloc[i][1] - average) ** 2

		if sst != 0:
			print ("R-Squared value: " + str(round(1 - (sse / sst), 2)))

	def getMinMax(self, data):
		minX = float("inf")
		maxX = float("-inf")
		for i in range(0, data.shape[0]):
			if data.iloc[i][0] < minX:
				minX = data.iloc[i][0]
			if data.iloc[i][0] > maxX:
				maxX = data.iloc[i][0]
		return [minX, maxX]

	def calculateTheta0AndThetha1(self, data):
		minMax = self.getMinMax(data)
		minX = minMax[0]
		maxX = minMax[1]

		data = self.dataScaling(minX, maxX, data)
		m = len(data)
		alpha = 0.1

		self.theta0 = 0
		self.theta1 = 0

		iterations = 0
		costFunction = 1000
		while costFunction > 0.0001:
			iterations += 1
			sumT0 = 0.0
			sumT1 = 0.0

			for i in range(0, m):
				cost = self.estimate(data[i][0]) - data[i][1]
				sumT0 += cost
				sumT1 += cost * data[i][0]
			self.theta0 = self.theta0 - alpha * (1 / m) * sumT0
			self.theta1 = self.theta1 - alpha * (1 / m) * sumT1
			costFunction = (1 / (2 * m)) * pow(sumT0, 2)

		if maxX != 0 and minX != 0:
			self.theta1 = (self.theta1 / (maxX - minX))

		print("t0 = " + str(round(self.theta0, 4)))
		print("t1 = " + str(round(self.theta1, 4)))
		print("Nb of iterations: " + str(iterations))


