import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import sys
from LinearRegression import LinearRegression

import pandas as pd

if len(sys.argv) < 2:
	print ("usage: python chart.py dataset.csv")
	sys.exit()

ln = LinearRegression()

try:
	ln.load()
except ValueError:
	sys.exit()
ln.setThetasFromFilename(sys.argv[1])

def linear_regression(value):
	print(value)
	return ln.estimate(value)

try:
	df = pd.read_csv(sys.argv[1])
except IOError as e:
	print ("Cannot open or / and read the file '" + sys.argv[1] + "'")
	sys.exit()
except pd.errors.EmptyDataError:
	print ("Invalid data file '" + sys.argv[1] + "'")
	sys.exit()
except pd.errors.ParserError:
	print ("Invalid data file '" + sys.argv[1] + "'")
	sys.exit()
except UnicodeDecodeError:
	print ("Invalid data file '" + sys.argv[1] + "'")
	sys.exit()

if not ln.checkIntegrity(df):
	print ("Wrong dataset format. Please use .csv with ',' as separator")
	sys.exit()

bg_color = '#CCCCCC'
fg_color = 'black'

fig = plt.figure(facecolor=bg_color, edgecolor=fg_color)
axes = fig.add_subplot(111)
axes.patch.set_facecolor(bg_color)
axes.xaxis.set_tick_params(color=fg_color, labelcolor=fg_color)
axes.yaxis.set_tick_params(color=fg_color, labelcolor=fg_color)
for spine in axes.spines.values():
    spine.set_color(fg_color)

minMax = ln.getMinMax(df)
distance = abs(minMax[0] - minMax[1])

x = np.linspace(minMax[0] - distance / 10, minMax[1] + distance / 10, 1000)
plt.plot(x, ln.theta0 + ln.theta1 * x, linestyle='solid', color='#2E7D32')
sns.regplot(x = df[df.columns.values[0]], y = df[df.columns.values[1]], marker = ".", fit_reg = False, color=fg_color) 

plt.show()
