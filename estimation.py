import sys
import pickle

from LinearRegression import LinearRegression

ln = LinearRegression()
try:
	if ln.load() == 0:
		print("Please run the training script before")
except ValueError:
	sys.exit()
ln.selectThetas()

correct = False
while not correct:
	try:
		value = float(input('Enter a value to estimate: '))
		correct = True
	except ValueError:
		print ("ValueError: Please enter an number")

print ('Estimation: ' + str(round(ln.estimate(value), 5)) )